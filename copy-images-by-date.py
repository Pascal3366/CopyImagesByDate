#!/usr/bin/python3
import os, time, shutil, sys

def display_help():
    print("Usage: " + sys.argv[0] + " [Source Directory] " + " [Destination Directory]")

if len(sys.argv) < 3:
    display_help()
elif sys.argv[1] == '--help':
    display_help()
else:
   sourcedir = sys.argv[1]
   destdir = sys.argv[2]

   print("Source Directory: %s" % sourcedir)
   print("Destination Directory: %s" % destdir)

   os.chdir(sourcedir)
   for f in os.listdir('.'):
      ftime = time.gmtime(os.path.getmtime(f))
      ctime_dir = destdir + '/' + str(ftime.tm_year)
      if not os.path.isdir(ctime_dir):
         os.mkdir(ctime_dir)
      dst = ctime_dir + '/' + f
      shutil.copy(f, dst);
      print('File' + f + 'has been copied to' + dst)
